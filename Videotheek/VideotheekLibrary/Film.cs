﻿using System;
using System.ComponentModel;

namespace VideotheekLibrary
{
    public class Film : INotifyPropertyChanged
    {
        public Film()
        {

        }
        private int _inVoorraad;
        private int _uitVoorraad;
        private int _totaalVerhuurd;

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public bool Changed { get; set; }
        public int BandNr { get; set; }
        public string Titel { get; set; }

        public int GenreNr { get; set; }
        public string Genre { get; set; }

        public int InVoorraad
        {
            get { return _inVoorraad; }
            set
            {
                _inVoorraad = value;
                Changed = true;
                RaisePropertyChanged("InVoorraad");
            }
        }
        public int UitVoorraad
        {
            get { return _uitVoorraad; }
            set
            {
                _uitVoorraad = value;
                Changed = true;
                RaisePropertyChanged("UitVoorraad");
            }

        }
        public decimal Prijs { get; set; }

        public int TotaalVerhuurd
        {
            get { return _totaalVerhuurd; }
            set
            {
                _totaalVerhuurd = value;
                Changed = true;
                RaisePropertyChanged("TotaalVerhuurd");
            }
        }

        public Film(int bandnr, string titel, int genrenr, string genre, int invoorraad,
            int uitvoorraad, decimal prijs, int totaalverhuur)
        {
            BandNr = bandnr;
            Titel = titel;
            GenreNr = genrenr;
            Genre = genre;
            InVoorraad = invoorraad;
            UitVoorraad = uitvoorraad;
            Prijs = prijs;
            TotaalVerhuurd = totaalverhuur;
            Changed = false;
        }
    }
}