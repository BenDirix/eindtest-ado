﻿using System.Globalization;
using System.Windows.Controls;

namespace VideotheekLibrary
{
    public class IntGroterDanNul : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int getal;

            if (value == null || value.ToString() == string.Empty)
                return new ValidationResult(false, "Getal moet ingevuld zijn");

            if (!int.TryParse(value.ToString(), out getal))
                return new ValidationResult(false, "Waarde moet een getal zijn");

            if (getal <= 0)
                return new ValidationResult(false, "Getal moet groter dan nul zijn");

            return ValidationResult.ValidResult;
        }
    }
}
