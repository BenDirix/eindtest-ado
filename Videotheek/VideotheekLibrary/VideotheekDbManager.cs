﻿using System;
using System.Configuration;
using System.Data.Common;
using System.Windows;

namespace VideotheekLibrary
{
    public class VideotheekDbManager
    {
        private static ConnectionStringSettings conVideoSetting = ConfigurationManager.ConnectionStrings["Videotheek"];
        private static DbProviderFactory factory = DbProviderFactories.GetFactory(conVideoSetting.ProviderName);

        public DbConnection GetConnection()
        {
            DbConnection conVideo;

            conVideo = factory.CreateConnection();
            conVideo.ConnectionString = conVideoSetting.ConnectionString;            

            return conVideo;
        }
    }
}
