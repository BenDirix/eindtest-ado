﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace VideotheekLibrary
{
    public class FilmManager
    {

        public ObservableCollection<Film> GetFilms()
        {
            ObservableCollection<Film> films = new ObservableCollection<Film>();
            var manager = new VideotheekDbManager();

            using (var conVideo = manager.GetConnection())
            {
                using (var comFilm = conVideo.CreateCommand())
                {
                    comFilm.CommandType = CommandType.Text;
                    comFilm.CommandText = "Select BandNr, Titel, Films.GenreNr as GenreNr, Genres.Genre as Genre, InVoorraad, UitVoorraad, " +
                        "Prijs, TotaalVerhuurd FROM Films INNER JOIN Genres On Films.GenreNr = Genres.GenreNr order by Titel";

                    conVideo.Open();
                    using (var rdrFilm = comFilm.ExecuteReader())
                    {
                        int bandnrPos = rdrFilm.GetOrdinal("BandNr");
                        int titelPos = rdrFilm.GetOrdinal("Titel");
                        int genrenrPos = rdrFilm.GetOrdinal("GenreNr");
                        int genrePos = rdrFilm.GetOrdinal("Genre");
                        int invoorraadPos = rdrFilm.GetOrdinal("InVoorraad");
                        int uitvoorraadPos = rdrFilm.GetOrdinal("UitVoorraad");
                        int prijsPos = rdrFilm.GetOrdinal("Prijs");
                        int totaalverhuurdPos = rdrFilm.GetOrdinal("TotaalVerhuurd");

                        while (rdrFilm.Read())
                        {
                            films.Add(new Film(
                                rdrFilm.GetInt32(bandnrPos),
                                rdrFilm.GetString(titelPos),
                                rdrFilm.GetInt32(genrenrPos),
                                rdrFilm.GetString(genrePos),
                                rdrFilm.GetInt32(invoorraadPos),
                                rdrFilm.GetInt32(uitvoorraadPos),
                                rdrFilm.GetDecimal(prijsPos),
                                rdrFilm.GetInt32(totaalverhuurdPos)));
                        }
                    }//rdrFilm
                }//comFilm
            }//conVideo

            return films;
        }
        public List<string> GetGenres()
        {
            List<string> genres = new List<string>();
            var manager = new VideotheekDbManager();

            using (var conVideo = manager.GetConnection())
            {
                using (var comGenres = conVideo.CreateCommand())
                {
                    comGenres.CommandType = CommandType.Text;
                    comGenres.CommandText = "select Genre from Genres order by Genre";

                    conVideo.Open();
                    using (var rdrGenre = comGenres.ExecuteReader())
                    {
                        int genrePos = rdrGenre.GetOrdinal("Genre");
                        while (rdrGenre.Read())
                        {
                            genres.Add(rdrGenre.GetString(genrePos));
                        }
                    }//rdrGenre
                }//comGenres
            }//conVideo

            return genres;
        }

        public List<Film> SchrijfToevoegingen(List<Film> filmsList)
        {
            List<Film> nietToegevoegdeFilms = new List<Film>();
            var manager = new VideotheekDbManager();
            using (var conVideo = manager.GetConnection())
            {
                using (var comInsert = conVideo.CreateCommand())
                {
                    comInsert.CommandType = CommandType.Text;
                    comInsert.CommandText = "Insert into Films (Titel, GenreNr, InVoorraad, UitVoorraad, Prijs, TotaalVerhuurd) " +
                        "values (@titel, (Select Genres.GenreNr From Genres where Genres.Genre = @genre), @inVoorraad, @uitVoorraad, @prijs, @totaalVerhuurd)";

                    var parTitel = comInsert.CreateParameter();
                    parTitel.ParameterName = "@titel";
                    comInsert.Parameters.Add(parTitel);

                    var parGenre = comInsert.CreateParameter();
                    parGenre.ParameterName = "@genre";
                    comInsert.Parameters.Add(parGenre);

                    var parInVoorraad = comInsert.CreateParameter();
                    parInVoorraad.ParameterName = "@inVoorraad";
                    comInsert.Parameters.Add(parInVoorraad);

                    var parUitVoorraad = comInsert.CreateParameter();
                    parUitVoorraad.ParameterName = "@uitVoorraad";
                    comInsert.Parameters.Add(parUitVoorraad);

                    var parPrijs = comInsert.CreateParameter();
                    parPrijs.ParameterName = "@prijs";
                    comInsert.Parameters.Add(parPrijs);

                    var parTotaalVerhuurd = comInsert.CreateParameter();
                    parTotaalVerhuurd.ParameterName = "@totaalVerhuurd";
                    comInsert.Parameters.Add(parTotaalVerhuurd);

                    conVideo.Open();

                    foreach (Film film in filmsList)
                    {
                        try
                        {
                            parTitel.Value = film.Titel;
                            parInVoorraad.Value = film.InVoorraad;
                            parUitVoorraad.Value = film.UitVoorraad;
                            parPrijs.Value = film.Prijs;
                            parTotaalVerhuurd.Value = film.TotaalVerhuurd;
                            parGenre.Value = film.Genre;

                            if (comInsert.ExecuteNonQuery() == 0)
                                nietToegevoegdeFilms.Add(film);
                        }
                        catch (Exception)
                        {
                            nietToegevoegdeFilms.Add(film);
                        }
                    }//foreach
                }//comInsert
            }//conVideo
            return nietToegevoegdeFilms;
        }
        public List<Film> SchrijfVerwijderingen(List<Film> filmsList)
        {
            List<Film> nietVerwijderdeFilms = new List<Film>();
            var manager = new VideotheekDbManager();
            using (var conVideo = manager.GetConnection())
            {
                using (var comDelete = conVideo.CreateCommand())
                {
                    comDelete.CommandType = CommandType.Text;
                    comDelete.CommandText = "delete from Films where BandNr = @bandNr";

                    var parBandNr = comDelete.CreateParameter();
                    parBandNr.ParameterName = "@bandNr";
                    comDelete.Parameters.Add(parBandNr);

                    conVideo.Open();
                    foreach (Film film in filmsList)
                    {
                        try
                        {
                            parBandNr.Value = film.BandNr;

                            if (comDelete.ExecuteNonQuery() == 0)
                                nietVerwijderdeFilms.Add(film);
                        }
                        catch (Exception)
                        {
                            nietVerwijderdeFilms.Add(film);
                        }
                    }//foreach
                }//comDelete
            }//conVideo

            return nietVerwijderdeFilms;
        }
        public List<Film> SchrijfWijzigingen(List<Film> filmsList)
        {
            List<Film> nietGewijzigdeFilms = new List<Film>();
            var manager = new VideotheekDbManager();
            using (var conVideo = manager.GetConnection())
            {
                using (var comUpdate = conVideo.CreateCommand())
                {
                    comUpdate.CommandType = CommandType.Text;
                    comUpdate.CommandText = "Update Films set InVoorraad = @inVoorraad, UitVoorraad = @uitVoorraad, TotaalVerhuurd = @totaalVerhuurd where BandNr = @BandNr";

                    var parBandNr = comUpdate.CreateParameter();
                    parBandNr.ParameterName = "@bandNr";
                    comUpdate.Parameters.Add(parBandNr);

                    var parInVoorraad = comUpdate.CreateParameter();
                    parInVoorraad.ParameterName = "@inVoorraad";
                    comUpdate.Parameters.Add(parInVoorraad);

                    var parUitVoorraad = comUpdate.CreateParameter();
                    parUitVoorraad.ParameterName = "@uitVoorraad";
                    comUpdate.Parameters.Add(parUitVoorraad);

                    var parTotaalVerhuurd = comUpdate.CreateParameter();
                    parTotaalVerhuurd.ParameterName = "@totaalVerhuurd";
                    comUpdate.Parameters.Add(parTotaalVerhuurd);

                    conVideo.Open();

                    foreach (Film film in filmsList)
                    {
                        try
                        {
                            parBandNr.Value = film.BandNr;
                            parInVoorraad.Value = film.InVoorraad;
                            parUitVoorraad.Value = film.UitVoorraad;
                            parTotaalVerhuurd.Value = film.TotaalVerhuurd;

                            if (comUpdate.ExecuteNonQuery() == 0)
                                nietGewijzigdeFilms.Add(film);
                        }
                        catch (Exception)
                        {
                            nietGewijzigdeFilms.Add(film);
                        }
                    }//foreach
                }//comUpdate
            }//conVideo

            return nietGewijzigdeFilms;
        }
    }
}
