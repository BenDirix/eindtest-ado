﻿using System.Globalization;
using System.Windows.Controls;

namespace VideotheekLibrary
{
    public class VeldMoetIngevuldZijn : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null || value.ToString() == string.Empty)
                return new ValidationResult(false, "Veld moet ingevuld zijn");

            return ValidationResult.ValidResult;
        }
    }
}
