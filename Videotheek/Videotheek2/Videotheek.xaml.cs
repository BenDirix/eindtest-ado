﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using VideotheekLibrary;

namespace Videotheek2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CollectionViewSource filmViewSource;
        public ObservableCollection<Film> filmsOb = new ObservableCollection<Film>();
        public List<Film> verwijderdeFilms = new List<Film>();
        public List<Film> toegevoegdeFilms = new List<Film>();
        public List<Film> gewijzigdeFilms = new List<Film>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            VulData();
        }

        private void VulData()
        {
            try
            {
                var manager = new FilmManager();
                List<string> genreLijst = manager.GetGenres();
                genreLijst.Insert(0, "");
                genreComboBox.ItemsSource = genreLijst;

                filmViewSource = ((CollectionViewSource)(this.FindResource("filmViewSource")));
                // Load data by setting the CollectionViewSource.Source property:
                // filmViewSource.Source = [generic data source]

                filmsOb = manager.GetFilms();
                filmViewSource.Source = filmsOb;
                listBoxFilms.SelectedIndex = 0;

                filmsOb.CollectionChanged += this.OnCollectionChanged;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (Film oudeFilm in e.OldItems)                
                    verwijderdeFilms.Add(oudeFilm);                
            }
            if (e.NewItems != null)
            {
                foreach (Film nieuweFilm in e.NewItems)                
                    toegevoegdeFilms.Add(nieuweFilm);                
            }
        }
        private void ButtonSwitch()
        {
            if(buttonToevoegen.Content.ToString() == "Toevoegen")
            {
                buttonToevoegen.Content = "Bevestigen";
                buttonVerwijderen.Content = "Annuleren";
                buttonOpslaan.Visibility = Visibility.Hidden;
                buttonVerhuur.Visibility = Visibility.Hidden;
                listBoxFilms.IsEnabled = false;
                titelTextBox.IsReadOnly = false;
                genreComboBox.IsEnabled = true;
                inVoorraadTextBox.IsReadOnly = false;
                uitVoorraadTextBox.IsReadOnly = false;
                prijsTextBox.IsReadOnly = false;
                totaalVerhuurdTextBox.IsReadOnly = false;
            }
            else
            {
                buttonToevoegen.Content = "Toevoegen";
                buttonVerwijderen.Content = "Verwijderen";
                buttonOpslaan.Visibility = Visibility.Visible;
                buttonVerhuur.Visibility = Visibility.Visible;
                listBoxFilms.IsEnabled = true;
                titelTextBox.IsReadOnly = true;
                genreComboBox.IsEnabled = false;
                inVoorraadTextBox.IsReadOnly = true;
                uitVoorraadTextBox.IsReadOnly = true;
                prijsTextBox.IsReadOnly = true;
                totaalVerhuurdTextBox.IsReadOnly = true;                
            }
        }
        private void ButtonVerhuur_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxFilms.SelectedItem != null)
            {
                Film geselecteerdeFilm = (Film)listBoxFilms.SelectedItem;
                if (geselecteerdeFilm.InVoorraad <= 0)
                    MessageBox.Show("Alle films zijn verhuurd!", "Verhuur", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.OK);
                else
                {
                    geselecteerdeFilm.InVoorraad -= 1;
                    geselecteerdeFilm.UitVoorraad += 1;
                    geselecteerdeFilm.TotaalVerhuurd += 1;
                }
            }
        }

        private void ButtonToevoegen_Click(object sender, RoutedEventArgs e)
        {
            Film nieuweFilm = new Film();
            if (buttonToevoegen.Content.ToString() == "Toevoegen")
            {                
                filmsOb.Add(nieuweFilm);
                listBoxFilms.SelectedIndex = filmsOb.IndexOf(nieuweFilm);

                ButtonSwitch();
            }
            else
            {
                titelTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                genreComboBox.GetBindingExpression(ComboBox.SelectedItemProperty).UpdateSource();
                inVoorraadTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                uitVoorraadTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                prijsTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                totaalVerhuurdTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();

                bool foutGevonden = false;
                foreach (var c in grid2.Children)
                {
                    if (c is AdornerDecorator)
                    {
                        if (Validation.GetHasError(((AdornerDecorator)c).Child))
                            foutGevonden = true;
                    }
                    else
                    {
                        if (Validation.GetHasError((DependencyObject)c))
                            foutGevonden = true;
                    }
                }
                
                if (foutGevonden)
                    MessageBox.Show("Alle velden moeten ingevuld zijn!", "Waarschuwing",MessageBoxButton.OK,MessageBoxImage.Error);                
                else
                {                    
                    listBoxFilms.SelectedIndex = 0;
                    ButtonSwitch();                    
                }
            }            
        }

        private void ButtonVerwijderen_Click(object sender, RoutedEventArgs e)
        {
            if (buttonVerwijderen.Content.ToString() == "Annuleren")
            {
                ButtonSwitch();
                listBoxFilms.SelectedIndex = 0;
                filmsOb.RemoveAt(filmsOb.Count - 1);
            }
            else
            {
                if(listBoxFilms.SelectedItem != null)
                {
                    Film geselecteerdeFilm = (Film)listBoxFilms.SelectedItem;
                    if(geselecteerdeFilm.BandNr != 0)
                    {
                        if (MessageBox.Show("Bent u zeker dat u deze film wilt verwijderen?", "Verwijderen",
                                MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                        {
                            filmsOb.RemoveAt(listBoxFilms.SelectedIndex);
                        }
                    }
                    else
                    {
                        MessageBox.Show("U moet eerst alles opslaan voor u deze film kan verwijderen");
                    }
                }
            }
                
        }

        private void ButtonOpslaan_Click(object sender, RoutedEventArgs e)
        {
            var manager = new FilmManager();
            List<Film> resultaatFilms = new List<Film>();
            StringBuilder boodschap = new StringBuilder();

            if (MessageBox.Show("Wilt u alles wegschrijven naar de database?", "Opslaan", 
                MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
            {                             
                if (toegevoegdeFilms.Count != 0)
                {
                    resultaatFilms = manager.SchrijfToevoegingen(toegevoegdeFilms);
                    if (resultaatFilms.Count > 0)
                    {
                        boodschap.Append("Niet toegevoegd: \n");
                        foreach (var film in resultaatFilms)
                            boodschap.Append($"{film.Titel}\n");                        
                    }
                    else
                        boodschap.Append($"{toegevoegdeFilms.Count - resultaatFilms.Count} film(s) toegevoegd aan de database\n");
                           
                }
                resultaatFilms.Clear();
                if (verwijderdeFilms.Count != 0)
                {
                    resultaatFilms = manager.SchrijfVerwijderingen(verwijderdeFilms);
                    if (resultaatFilms.Count > 0)
                    {
                        boodschap.Append("Niet verwijderd:\n");
                        foreach (var film in resultaatFilms)
                            boodschap.Append($"{film.Titel}\n");                        
                    }
                    else
                        boodschap.Append($"{verwijderdeFilms.Count - resultaatFilms.Count} film(s) verwijderd uit de databse\n");
                }
                resultaatFilms.Clear();
                foreach (var film in filmsOb)
                {
                    if ((film.Changed == true) && (film.BandNr != 0))
                    {
                        gewijzigdeFilms.Add(film);
                        film.Changed = false;
                    }
                }
                if (gewijzigdeFilms.Count != 0)
                {
                    resultaatFilms = manager.SchrijfWijzigingen(gewijzigdeFilms);
                    if (resultaatFilms.Count > 0)
                    {
                        boodschap.Append("Niet gewijzigd:\n");
                        foreach (var film in resultaatFilms)
                            boodschap.Append($"{film.Titel}\n");                        
                    }
                    else
                        boodschap.Append($"{gewijzigdeFilms.Count - resultaatFilms.Count} film(s) gewijzigd in de database\n");
                }

                if (boodschap.Length != 0)
                    MessageBox.Show(boodschap.ToString(), "Info", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                else
                    MessageBox.Show("Niets aangepast", "Info", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);

                gewijzigdeFilms.Clear();
                toegevoegdeFilms.Clear();
                verwijderdeFilms.Clear();
                VulData();
            }
        }
    }
}
